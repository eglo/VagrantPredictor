﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Management.Automation;
using System.Management.Automation.Subsystem;
using System.Management.Automation.Subsystem.Prediction;
using System.Text.RegularExpressions;
using System.Management.Automation.Language;
using System.Runtime.InteropServices;

namespace VagrantPredictor {
    public class VagrantPredictor : ICommandPredictor {

        /// <summary>
        /// Gets the unique identifier of a subsystem implementation.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Gets the name of a subsystem implementation.
        /// </summary>
        public string Name => "VagrantPredictor";
        
        /// <summary>
        /// Gets the description of a subsystem implementation.
        /// </summary>
        public string Description => "A predictor for the Vagrant application";

        public readonly Command vagrantCommandList = CommandList.VagrantCommand;

        internal VagrantPredictor(string guid) {
            Id = new Guid(guid);
        }

        /// <summary>
        /// Get the predictive suggestions. It indicates the start of a suggestion rendering session.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="context">The <see cref="PredictionContext"/> object to be used for prediction.</param>
        /// <param name="cancellationToken">The cancellation token to cancel the prediction.</param>
        /// <returns>An instance of <see cref="SuggestionPackage"/>.</returns>
        public SuggestionPackage GetSuggestion(PredictionClient client, PredictionContext context, CancellationToken cancellationToken) {
            
            // Get the entire current input string.
            string input = context.InputAst.Extent.Text;

            // If the current input is null or does not have a leading token of "vagrant" then return no suggestions.
            if (string.IsNullOrWhiteSpace(input) || !Regex.Match(context.InputTokens[0].Text, @"(^|[\\/])vagrant((\.exe$)|$)").Success) {
                return default;
            }

            // Initialize the list of suggestions to fill and return.
            List<PredictiveSuggestion> suggestions = new();

            // At this point we know that we have "vagrant" as the starting point, so set the currentCommand to the root vagrant command.
            Command currentCommand = vagrantCommandList;

            // Retrieve the trailing command on the stack. This is (count - 2) because InputTokens always has a trailing null token.
            string trailingCommandString = context.InputTokens[context.InputTokens.Count - 2].Text;
            if(context.InputTokens.Count == 2) {
                if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                    trailingCommandString = context.InputTokens[context.InputTokens.Count - 2].Text.Split('\\').Last();
                } else {
                    trailingCommandString = context.InputTokens[context.InputTokens.Count - 2].Text.Split('/').Last();
                }
            }

            // Start looping through all tokens, skipping the first token as it must be "vagrant" at this point.
            // Token List: vagrant box add test --cacert C:\test ...          nulltoken
            // Indices:       0     1   2   3       4        5   ... context.InputTokens.count - 1
            foreach (Token token in context.InputTokens.Skip(1)) {

                // Attempt to parse the token into a subcommand of the current command.
                Command? iteratedCommand = currentCommand.SubCommands.FirstOrDefault(c => c.CommandString.Equals(token.Text, StringComparison.OrdinalIgnoreCase) || c.AltCommandStrings.Contains(token.Text));

                // If the current iterated command is not null and either has subcommands or is immediately requiring additional input, set it as the current command.
                if (
                    iteratedCommand is not null
                    && (
                         (iteratedCommand.SubCommands.Count > 0) || iteratedCommand.IsTerminating || (
                             context.TokenAtCursor is null
                             && !string.IsNullOrEmpty(iteratedCommand.InputHint)
                             && (
                                 iteratedCommand.CommandString.Equals(trailingCommandString, StringComparison.OrdinalIgnoreCase)
                                 || iteratedCommand.AltCommandStrings.Contains(trailingCommandString)
                             )
                         )
                    )
                ) {
                    currentCommand = iteratedCommand;
                }
            }

            /* 
             * Return default if:
             * - The current command does not take input and one of the following is true:
             *     - The trailing command on the stack is not the current command or the beginning of one of its subcommands, and the immediate parent command of the trailing command does not take input.
             *     - The current command has no subcommands
             * 
             * This is to prevent predictions from unknown commands being shown. i.e. "vagrant notacommand" is not a valid command, so we shouldn't show predictions after it.
             */
            if (
                string.IsNullOrEmpty(currentCommand.InputHint)
                && (
                    (
                        !(
                            currentCommand.CommandString.Equals(trailingCommandString, StringComparison.OrdinalIgnoreCase)
                            || currentCommand.AltCommandStrings.Contains(trailingCommandString)
                        )
                        &&
                        !currentCommand.SubCommands.Any(c =>
                            c.CommandString.StartsWith(trailingCommandString, StringComparison.OrdinalIgnoreCase)
                            || c.AltCommandStrings.Any(ac => ac.StartsWith(trailingCommandString))
                        )
                        &&
                        !currentCommand.SubCommands.Any(c =>
                            (
                                c.CommandString.Equals(context.InputTokens[context.InputTokens.Count - 3].Text, StringComparison.OrdinalIgnoreCase)
                                || c.AltCommandStrings.Contains(context.InputTokens[context.InputTokens.Count - 3].Text)
                            )
                            && !string.IsNullOrEmpty(c.InputHint)
                        )
                    )
                    || (!currentCommand.SubCommands.Any())
                )
            ) {
                return default;
            }
            if (context.TokenAtCursor is null) { // vagrant box add --provider |<--- cursor is here
                // If the current command needs input and it is the trailing command, suggest the input string hint.
                if (
                    !string.IsNullOrEmpty(currentCommand.InputHint)
                    && (
                        currentCommand.CommandString.Equals(trailingCommandString, StringComparison.OrdinalIgnoreCase)
                        || currentCommand.AltCommandStrings.Contains(trailingCommandString)
                    )
                ) {
                    suggestions.Add(new PredictiveSuggestion(string.Concat(input, currentCommand.InputHint)));
                } else { // Otherwise, suggest the list of subcommands for the current command that have not already been typed.
                    suggestions.AddRange(currentCommand.SubCommands
                    .Where(c => !(
                        context.InputTokens.Select(t => t.Text).Contains(c.CommandString)
                        || context.InputTokens.Select(t => t.Text).Where(t => c.AltCommandStrings.Contains(t)).Any()
                    ))
                    .Select(c => new PredictiveSuggestion(string.Concat(input, c.CommandString), c.Description)));
                }
            } else { // Cursor is currently on a token.
                // Suggest the list of subcommands from the current command that start with the token the cursor is on and that have not already been typed.
                // Predictions will be a substring of the subcommand based on the length of the current token.
                suggestions.AddRange(currentCommand.SubCommands
                    .Where(c =>
                        c.CommandString.StartsWith(context.TokenAtCursor.Text, StringComparison.OrdinalIgnoreCase)
                        && !(
                            context.InputTokens.Select(t => t.Text).Contains(c.CommandString)
                            || context.InputTokens.Select(t => t.Text).Where(t => c.AltCommandStrings.Contains(t)).Any()
                        )
                    )
                    .Select(c => new PredictiveSuggestion(string.Concat(input, c.CommandString[context.TokenAtCursor.Text.Length..]), c.Description))
                );
            }
            return new SuggestionPackage(suggestions);
        }

        #region Unimplemented
        /// <summary>
        /// Gets a value indicating whether the predictor accepts a specific kind of feedback.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="feedback">A specific type of feedback.</param>
        /// <returns>True or false, to indicate whether the specific feedback is accepted.</returns>
        public bool CanAcceptFeedback(PredictionClient client, PredictorFeedbackKind feedback) => false;

        /// <summary>
        /// One or more suggestions provided by the predictor were displayed to the user.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="session">The mini-session where the displayed suggestions came from.</param>
        /// <param name="countOrIndex">
        /// When the value is greater than 0, it's the number of displayed suggestions from the list
        /// returned in <paramref name="session"/>, starting from the index 0. When the value is
        /// less than or equal to 0, it means a single suggestion from the list got displayed, and
        /// the index is the absolute value.
        /// </param>
        public void OnSuggestionDisplayed(PredictionClient client, uint session, int countOrIndex) { }

        /// <summary>
        /// The suggestion provided by the predictor was accepted.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="session">Represents the mini-session where the accepted suggestion came from.</param>
        /// <param name="acceptedSuggestion">The accepted suggestion text.</param>
        public void OnSuggestionAccepted(PredictionClient client, uint session, string acceptedSuggestion) { }

        /// <summary>
        /// A command line was accepted to execute.
        /// The predictor can start processing early as needed with the latest history.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="history">History command lines provided as references for prediction.</param>
        public void OnCommandLineAccepted(PredictionClient client, IReadOnlyList<string> history) { }

        /// <summary>
        /// A command line was done execution.
        /// </summary>
        /// <param name="client">Represents the client that initiates the call.</param>
        /// <param name="commandLine">The last accepted command line.</param>
        /// <param name="success">Shows whether the execution was successful.</param>
        public void OnCommandLineExecuted(PredictionClient client, string commandLine, bool success) { }
        #endregion
    }
}
