﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VagrantPredictor {
    public static class CommandList {

        #region SharedCommands

        private static readonly List<Command> SharedCommands = new() {
            new("--color",             "Enable color output"),
            new("--no-color",          "Disable color output"),
            new("--machine-readable",  "Enable machine readable output"),
            new("--version",           "Display Vagrant version", null, "", new[] {"-v"}),
            new("--debug",             "Enable debug output"),
            new("--timestamp",         "Enable timestamps on log output"),
            new("--debug-timestamp",   "Enable debug output with timestamps"),
            new("--no-tty",            "Enable non-interactive output"),
            new("--help",              "Print help",              null, "", new[] {"-h"})
        };

        #endregion

        #region AutocompleteCommand

        private static readonly Command AutocompleteCommand = new(
            "autocomplete",
            "manages autocomplete installation on host",
            new List<Command>() {
                new Command(
                    "install",
                    "install autocomplete on host",
                    new List<Command>() {
                        new Command("--bash", "Install bash autocomplete", null, "", new[] {"-b"}),
                        new Command("--zsh",  "Install zsh autocomplete",  null, "", new[] {"-z"})
                    }.Concat(SharedCommands).ToList()
                ),
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region BoxCommand

        private static readonly Command BoxCommand = new(
            "box",
            "manages boxes: installation, removal, etc.",
            new List<Command>() {
                new Command(
                    "add",
                    "add a box with the given address to Vagrant",
                    new List<Command>() {
                        new Command("--clean",            "Clean any temporary download files",        null, "", new[] {"-c"}),
                        new Command("--force",            "Overwrite an existing box if it exists",    null, "", new[] {"-f"}),
                        new Command("--insecure",         "Do not validate SSL certificates"),
                        new Command("--cacert",           "CA certificate for SSL download",           null, "<file>"),
                        new Command("--capath",           "CA certificate directory for SSL download", null, "<directory>"),
                        new Command("--cert",             "A client SSL cert, if needed",              null, "<file>"),
                        new Command("--location-trusted", "Trust 'Location' header from HTTP redirects and use the same credentials for subsequent urls as for the initial one"),
                        new Command("--provider",         "Provider the box should satisfy",           null, "<provider>"),
                        new Command("--box-version",      "Constrain version of the added box",        null, "<version>"),
                        new Command("--checksum",         "Checksum for the box",                      null, "<checksum>"),
                        new Command("--checksum-type",    "Checksum type (md5, sha1, sha256)",         null, "<type>"),
                        new Command("--name",             "Name of the box",                           null, "<box>")
                    }.Concat(SharedCommands).ToList(),
                    "<name, url, or path>"
                ),
                new Command(
                    "list",
                    "list all the boxes that are installed into Vagrant",
                    new List<Command>() {
                        new Command("--box-info", "Displays additional information about the boxes", null, "", new[] {"-i"})
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "outdated",
                    "shows whether or not the box you are using in your current Vagrant environment is outdated",
                    new List<Command>() {
                        new Command("--global",   "Check all boxes installed"),
                        new Command("--force",    "Force checks for latest box updates",       null, "", new[] {"-f"}),
                        new Command("--insecure", "Do not validate SSL certificates"),
                        new Command("--cacert",   "CA certificate for SSL download",           null, "<file>"),
                        new Command("--capath",   "CA certificate directory for SSL download", null, "<directory>"),
                        new Command("--cert",     "A client SSL cert, if needed",              null, "<file>"),
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "prune",
                    "remove old versions of installed boxes",
                    new List<Command>() {
                        new Command("--provider",          "The specific provider type for the boxes to destroy.",                 null, "<provider>", new[] {"-p"}),
                        new Command("--dry-run",           "Only print the boxes that would be removed.",                          null, "",           new[] {"-n"}),
                        new Command("--name",              "The specific box name to check for outdated versions.",                null, "<name>"),
                        new Command("--force",             "Destroy without confirmation even when box is in use.",                null, "",           new[] {"-f"}),
                        new Command("--keep-active-boxes", "When combined with `--force`, will keep boxes still actively in use.", null, "",           new[] {"-k"})
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "remove",
                    "remove a box from Vagrant that matches the given name",
                    new List<Command>() {
                        new Command("--force",       "Remove without confirmation.",                     null, "", new[] {"-f"}),
                        new Command("--provider",    "The specific provider type for the box to remove", null, "<provider>"),
                        new Command("--box-version", "The specific version of the box to remove",        null, "<version>"),
                        new Command("--all",         "Remove all available versions of the box")
                    }.Concat(SharedCommands).ToList(),
                    "<name>"
                ),
                new Command(
                    "repackage",
                    "repackage the given box and puts it in the current directory so you can redistribute it",
                    new List<Command>().Concat(SharedCommands).ToList(),
                    "<name> <provider> <version>"
                ),
                new Command(
                    "update",
                    "update the box that is in use in the current Vagrant environment, if there any updates available.",
                    new List<Command>() {
                        new Command("--box",      "Update a specific box",                     null, "<box>"),
                        new Command("--provider", "Update box with specific provider",         null, "<provider>"),
                        new Command("--force",    "Overwrite an existing box if it exists",    null, "", new[] {"-f"}),
                        new Command("--insecure", "Do not validate SSL certificates"),
                        new Command("--cacert",   "CA certificate for SSL download",           null, "<file>"),
                        new Command("--capath",   "CA certificate directory for SSL download", null, "<directory>"),
                        new Command("--cert",     "A client SSL cert, if needed",              null, "<file>"),
                    }.Concat(SharedCommands).ToList()
                ),
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region InitCommand

        private static readonly Command InitCommand = new(
            "init",
            "initializes a new Vagrant environment by creating a Vagrantfile",
            new List<Command>() {
                new Command("--box-version", "Version of the box to add",                                                    null, "<version>"),
                new Command("--force",       "Overwrite existing Vagrantfile",                                               null, "", new[] {"-f"}),
                new Command("--minimal",     "Use minimal Vagrantfile template (no help comments). Ignored with --template", null, "", new[] {"-m"}),
                new Command("--output",      "Output path for the box. '-' for stdout",                                      null, "<file>"),
                new Command("--template",    "Path to custom Vagrantfile template",                                          null, "<file>")
            }.Concat(SharedCommands).ToList(),
            "<name [url]>"
        );

        #endregion

        #region CapCommand

        public static readonly Command CapCommand = new(
            "cap",
            "checks or executes arbitrary capabilities that Vagrant has for hosts, guests, and providers",
            new List<Command>() {
                new Command("--check",  "Only checks for a capability, does not execute"),
                new Command("--target", "Target guest to run against (if applicable)", null, "<target id>", new[] {"-t"})
            }.Concat(SharedCommands).ToList(),
            "<type> <name>"
        );

        #endregion

        #region CloudCommand
        private static readonly Command CloudCommand = new(
            "cloud",
            "manages everything related to Vagrant Cloud",
            new List<Command>() {
                new Command(
                    "auth", 
                    "For various authorization operations on Vagrant Cloud",
                    new List<Command>() {
                        new Command(
                            "login",
                            "authenticate with HashiCorp's Vagrant Cloud server",
                            new List<Command>() {
                                new Command("--check",       "check if you are logged in",                  null, "",                    new[] {"-c"}),
                                new Command("--description", "set description for the Vagrant Cloud token", null, "<description>",       new[] {"-d"}),
                                new Command("--username",    "Vagrant Cloud username or email address",     null, "<username_or_email>", new[] {"-u"}),
                                new Command("--token",       "set the Vagrant Cloud login token",           null, "<token>",             new[] {"-t"})
                            }.Concat(SharedCommands).ToList()
                        ),
                        new Command(
                            "logout",
                            "log out of the Vagrant Cloud",
                            new List<Command>().Concat(SharedCommands).ToList()
                        ),
                        new Command(
                            "whoami",
                            "validate your Vagrant Cloud token and print the user who it belongs to",
                            new List<Command>().Concat(SharedCommands).ToList(),
                            "<token>"
                        ),
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "box",
                    "For managing a Vagrant box entry on Vagrant Cloud",
                    new List<Command>() {
                        new Command(
                            "create",
                            "create a new box entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--description",       "Full description of the box",  null, "<description>",       new[] {"-d"}),
                                new Command("--short-description", "Short description of the box", null, "<short-description>", new[] {"-s"}),
                                new Command("--private",           "Makes box private",            null, "",                    new[] {"-p"}),
                                new Command("--no-private",        "Makes box public")
                            }.Concat(SharedCommands).ToList(),
                            "<box>"
                        ),
                        new Command(
                            "delete",
                            "permanently delete the given box entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--force",    "Do not prompt for deletion confirmation", null, "", new[] {"-f"}),
                                new Command("--no-force", "Prompt for deletion confirmation")
                            }.Concat(SharedCommands).ToList(),
                            "<box>"
                        ),
                        new Command(
                            "show",
                            "display information about the latest version for the given Vagrant box",
                            new List<Command>() {
                                new Command("--versions", "Display box information for a specific version (can be defined multiple times)", null, "<version>"),
                                new Command("--auth",     "Authenticate with Vagrant Cloud if required before searching"),
                                new Command("--no-auth",  "Do not Authenticate with Vagrant Cloud")
                            }.Concat(SharedCommands).ToList(),
                            "<box>"
                        ),
                        new Command(
                            "update",
                            "update an already created box on Vagrant Cloud with the given options",
                            new List<Command>() {
                                new Command("--description",       "Full description of the box",  null, "<description>",       new[] {"-d"}),
                                new Command("--short-description", "Short description of the box", null, "<short-description>", new[] {"-s"}),
                                new Command("--private",           "Makes box private",            null, "",                    new[] {"-p"}),
                                new Command("--no-private",        "Makes box public")
                            }.Concat(SharedCommands).ToList(),
                            "<box>"
                        )
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "provider",
                    "For managing a Vagrant box's provider options",
                    new List<Command>() {
                        new Command(
                            "create",
                            "create a new provider entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--checksum",      "Checksum of the box for this provider",                     null, "<checksum_value>", new[] {"-c"}),
                                new Command("--checksum-type", "Type of checksum used (md5, sha1, sha256, sha384, sha512)", null, "<type>"          , new[] {"-C"})
                            }.Concat(SharedCommands).ToList(),
                            "<box> <provider> <version> <url>"),
                        new Command(
                            "delete",
                            "delete a provider entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--force",    "Force deletion of box version provider without confirmation", null, "", new[] {"-f"}),
                                new Command("--no-force", "Request confirmation before deletion of box version provider")
                            }.Concat(SharedCommands).ToList(),
                            "<box> <provider> <version>"),
                        new Command(
                            "update",
                            "update an already created provider for a box on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--checksum",      "Checksum of the box for this provider",                     null, "<checksum_value>", new[] {"-c"}),
                                new Command("--checksum-type", "Type of checksum used (md5, sha1, sha256, sha384, sha512)", null, "<type>"          , new[] {"-C"})
                            }.Concat(SharedCommands).ToList(),
                            "<box> <provider> <version> <url>"),
                        new Command(
                            "upload",
                            "upload a Vagrant box file to Vagrant Cloud for the specified version and provider",
                            new List<Command>() {
                                new Command("--direct",    "Upload asset directly to backend storage", null, "", new[] {"-D"}),
                                new Command("--no-direct", "Do not upload asset directly to backend storage")
                            }.Concat(SharedCommands).ToList(),
                            "<box> <provider> <version> <file>")
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "publish",
                    "A complete solution for creating or updating a new box on Vagrant Cloud",
                    new List<Command>() {
                        new Command("--box-version",         "Version of box to create",                                  null, "<version>"),
                        new Command("--url",                 "Remote URL to download this provider",                      null, "<url>"),
                        new Command("--description",         "Full description of box",                                   null, "<description>",    new[] {"-d"}),
                        new Command("--version-description", "Description of the version to create",                      null, "<description>"),
                        new Command("--force",               "Disables confirmation to create or update box",             null, "",                 new[] {"-f"}),
                        new Command("--no-force",            "Enables confirmation to create or update box"),
                        new Command("--private",             "Makes box private",                                         null, "",                 new[] {"-p"}),
                        new Command("--no-private",          "Makes box public"),
                        new Command("--release",             "Releases box",                                              null, "",                 new[] {"-r"}),
                        new Command("--no-release",          "Prevents release of box"),
                        new Command("--short-description",   "Short description of the box",                              null, "<description>",    new[] {"-s"}),
                        new Command("--checksum",            "Checksum of the box for this provider",                     null, "<checksum_value>", new[] {"-c"}),
                        new Command("--checksum-type",       "Type of checksum used (md5, sha1, sha256, sha384, sha512)", null, "<type>",           new[] {"-C"}),
                        new Command("--direct-upload",       "Upload asset directly to backend storage"),
                        new Command("--no-direct-upload",    "Do not upload asset directly to backend storage")
                    }.Concat(SharedCommands).ToList(),
                    "<box> <version> <provider-name> [provider-file]"
                ),
                new Command(
                    "search",
                    "Search Vagrant Cloud for available boxes",
                    new List<Command>() {
                        new Command("--json",     "Formats results in JSON",                                                   null, "",           new[] {"-j"}),
                        new Command("--page",     "The page to display Default: 1",                                            null, "<page>"),
                        new Command("--short",    "Shows a simple list of box names",                                          null, "",           new[] {"-s"}),
                        new Command("--order",    "Order to display results ('desc' or 'asc') Default: 'desc'",                null, "<order>",    new[] {"-o"}),
                        new Command("--limit",    "Max number of search results Default: 25",                                  null, "<limit>",    new[] {"-l"}),
                        new Command("--provider", "Filter search results to a single provider. Defaults to all.",              null, "<provider>", new[] {"-p"}),
                        new Command("--sort-by",  "Field to sort results on (created, downloads, updated) Default: downloads", null, "<sort>"),
                        new Command("--auth",     "Authenticate with Vagrant Cloud if required before searching"),
                        new Command("--no-auth",  "Do not authenticate with Vagrant Cloud")
                    }.Concat(SharedCommands).ToList(),
                    "<query>"),
                new Command(
                    "version",
                    "For managing a Vagrant box's versions",
                    new List<Command>() {
                        new Command(
                            "create",
                            "Creates a version entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--description", "A description for this version", null, "<description>", new[] {"-d"})
                            }.Concat(SharedCommands).ToList(),
                            "<box> <version>"
                        ),
                        new Command(
                            "delete",
                            "delete a version entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--force",    "Force deletion without confirmation", null, "", new[] {"-f"}),
                                new Command("--no-force", "Confirm before deletion"),
                            }.Concat(SharedCommands).ToList(),
                            "<box> <version>"
                        ),
                        new Command(
                            "release",
                            "release a version entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--force",    "Release without confirmation", null, "", new[] {"-f"}),
                                new Command("--no-force", "Confirm before release"),
                            }.Concat(SharedCommands).ToList(),
                            "<box> <version>"
                        ),
                        new Command(
                            "revoke",
                            "",
                            new List<Command>() {
                                new Command("--force",    "Force revocation without confirmation", null, "", new[] {"-f"}),
                                new Command("--no-force", "Confirm before revocation"),
                            }.Concat(SharedCommands).ToList(),
                            "<box> <version>"
                        ),
                        new Command(
                            "update",
                            "update a version entry on Vagrant Cloud",
                            new List<Command>() {
                                new Command("--description", "A description for this version", null, "<description>", new[] {"-d"}),
                            }.Concat(SharedCommands).ToList(),
                            "<box> <version>"
                        ),
                    }.Concat(SharedCommands).ToList())
            }.Concat(SharedCommands).ToList()
        );
        #endregion

        #region ConnectCommand

        private static readonly Command ConnectCommand = new(
            "connect",
            "access to any Vagrant environment shared using `vagrant share`",
            new List<Command>() {
                new Command("--disable-static-ip", "No static IP, only a SOCKS proxy"),
                new Command("--static-ip",         "Manually override static IP chosen",  null, "<ip>"),
                new Command("--ssh",               "SSH into the remote machine"),
                new Command("--driver",            "Deprecated option for compatibility", null, "<driver>"),
                new Command("--share-password",    "Custom share password")
            }.Concat(SharedCommands).ToList(),
            "<name>"
        );

        #endregion

        #region DestroyCommand

        private static readonly Command DestroyCommand = new(
            "destroy",
            "stops and deletes all traces of the vagrant machine",
            new List<Command>() {
                new Command("--force",       "Destroy without confirmation", null, "", new[] {"-f"}),
                new Command("--parallel",    "Enable parallelism if provider supports it (automatically enables force)"),
                new Command("--no-parallel", "Disable parallelism"),
                new Command("--graceful",    "Gracefully poweroff of VM",    null, "", new[] {"-g"}),
            }.Concat(SharedCommands).ToList(),
            "<name|id>");

        #endregion

        #region DockerExecCommand

        private static readonly Command DockerExecCommand = new(
            "docker-exec",
            "attach to an already running docker container",
            new List<Command>() {
                new Command("--",               "The command to run against the docker image.", null, "<command> [args]", null, true),
                new Command("--detach",         "Run in the background"),
                new Command("--no-detach",      "Run in the foreground"),
                new Command("--interactive",    "Keep STDIN open even if not attached",         null, "",           new[] {"-i"}),
                new Command("--no-interactive", "Do not keep STDIN open"),
                new Command("--tty",            "Allocate a tty",                               null, "",           new[] {"-t"}),
                new Command("--user",           "User or UID",                                  null, "<user|UID>", new[] {"-u"}),
                new Command("--prefix",         "Prefix output with machine names"),
                new Command("--no-prefix",      "Do not prefix output with machine names")
            }.Concat(SharedCommands).ToList(),
            "<vm_name>"
            );

        #endregion

        #region DockerLogsCommand

        private static readonly Command DockerLogsCommand = new(
            "docker-logs",
            "outputs the logs from the Docker container",
            new List<Command>() {
                new Command("--follow",    "Continue streaming in log output"),
                new Command("--no-follow", "Do not stream in log output"),
                new Command("--prefix",    "Prefix output with machine names"),
                new Command("--no-prefix", "Do not prefix output with machine names")
            }.Concat(SharedCommands).ToList());

        #endregion

        #region DockerRunCommand

        private static readonly Command DockerRunCommand = new(
            "docker-run",
            "run a one-off command in the context of a container",
            new List<Command>() {
                new Command("--detach",         "Run in the background"),
                new Command("--no-detach",      "Run in the foreground"),
                new Command("--tty",            "Allocate a tty",                         null, "", new[] {"-t"}),
                new Command("--rm",             "Remove container after execution",       null, "", new[] {"-r"}),
                new Command("--no-rm",          "Do not remove container after execution")
            },
            "<command>");

        #endregion

        #region GlobalStatusCommand

        private static readonly Command GlobalStatusCommand = new(
            "global-status",
            "outputs status Vagrant environments for this user",
            new List<Command>() {
                new Command("--prune", "Prune invalid entries"),
            }.Concat(SharedCommands).ToList());

        #endregion

        #region HaltCommand

        private static readonly Command HaltCommand = new(
            "halt",
            "stops the vagrant machine",
            new List<Command>() {
                new Command("--force", "Prune invalid entries", null, "", new[] {"-f"})
            }.Concat(SharedCommands).ToList(),
            "<name|id>");

        #endregion

        #region HelpCommand

        private static readonly Command HelpCommand = new(
            "help",
            "shows the help for a subcommand",
            new List<Command>() {
                new Command("autocomplete",  "", isTerminating: true),
                new Command("box",           "", isTerminating: true),
                new Command("init",          "", isTerminating: true),
                new Command("cap",           "", isTerminating: true),
                new Command("cloud",         "", isTerminating: true),
                new Command("connect",       "", isTerminating: true),
                new Command("destroy",       "", isTerminating: true),
                new Command("docker-exec",   "", isTerminating: true),
                new Command("docker-logs",   "", isTerminating: true),
                new Command("docker-run",    "", isTerminating: true),
                new Command("global-status", "", isTerminating: true),
                new Command("halt",          "", isTerminating: true),
                new Command("list-commands", "", isTerminating: true),
                new Command("login",         "", isTerminating: true),
                new Command("package",       "", isTerminating: true),
                new Command("plugin",        "", isTerminating: true),
                new Command("port",          "", isTerminating: true),
                new Command("powershell",    "", isTerminating: true),
                new Command("provider",      "", isTerminating: true),
                new Command("provision",     "", isTerminating: true),
                new Command("push",          "", isTerminating: true),
                new Command("rdp",           "", isTerminating: true),
                new Command("reload",        "", isTerminating: true),
                new Command("resume",        "", isTerminating: true),
                new Command("rsync",         "", isTerminating: true),
                new Command("rsync-auto",    "", isTerminating: true),
                new Command("share",         "", isTerminating: true),
                new Command("snapshot",      "", isTerminating: true),
                new Command("ssh",           "", isTerminating: true),
                new Command("ssh-config",    "", isTerminating: true),
                new Command("status",        "", isTerminating: true),
                new Command("suspend",       "", isTerminating: true),
                new Command("up",            "", isTerminating: true),
                new Command("upload",        "", isTerminating: true),
                new Command("validate",      "", isTerminating: true),
                new Command("version",       "", isTerminating: true),
                new Command("winrm",         "", isTerminating: true),
                new Command("winrm-config",  "", isTerminating: true)
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region ListCommandsCommand

        private static readonly Command ListCommandsCommand = new(
            "list-commands",
            "outputs all available Vagrant subcommands, even non-primary ones",
            new List<Command>().Concat(SharedCommands).ToList()
        );

        #endregion

        #region LoginCommand

        private static readonly Command LoginCommand = new(
            "login",
            "WARNING: This command has been deprecated and aliased to `vagrant cloud auth login`",
            new List<Command>() {
                new Command("--check",       "check if you are logged in",                  null, "",                    new[] {"-c"}),
                new Command("--description", "set description for the Vagrant Cloud token", null, "<description>",       new[] {"-d"}),
                new Command("--username",    "Vagrant Cloud username or email address",     null, "<username_or_email>", new[] {"-u"}),
                new Command("--token",       "set the Vagrant Cloud login token",           null, "<token>",             new[] {"-t"})
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region PackageCommand

        private static readonly Command PackageCommand = new(
            "package",
            "packages a running vagrant environment into a box",
            new List<Command>() {
                new Command("--base",        "Name of a VM in VirtualBox to package as a base box (VirtualBox Only)", null, "<name>"),
                new Command("--output",      "Name of the file to output",                                            null, "<name>"),
                new Command("--include",     "Comma separated additional files to package with the box",              null, "<file,file...>"),
                new Command("--info",        "Path to a custom info.json file containing additional box information", null, "<file>"),
                new Command("--vagrantfile", "Vagrantfile to package with the box",                                   null, "<file>"),
            }.Concat(SharedCommands).ToList(),
            "<name|id>");

        #endregion

        #region PluginCommand

        private static readonly Command PluginCommand = new(
            "plugin",
            "manages plugins: install, uninstall, update, etc.",
            new List<Command>() {
                new Command(
                    "expunge",
                    "remove all user installed plugin information",
                    new List<Command>(){
                        new Command("--force",       "Do not prompt for confirmation"),
                        new Command("--local",       "Include plugins from local project for expunge"),
                        new Command("--local-only",  "Only expunge local project plugins"),
                        new Command("--global-only", "Only expunge global plugins"),
                        new Command("--reinstall",   "Reinstall current plugins after expunge")
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "install",
                    "install a plugin with the given name or file path",
                    new List<Command>(){
                        new Command("--entry-point",          "The name of the entry point file for loading the plugin.",     null, "<name>"),
                        new Command("--plugin-clean-sources", "Remove all plugin sources defined so far (including defaults)"),
                        new Command("--plugin-source",        "Add a RubyGems repository source",                             null, "<plugin_source>"),
                        new Command("--plugin-version",       "Install a specific version of the plugin",                     null, "<plugin_version>"),
                        new Command("--local",                "Install plugin for local project only"),
                        new Command("--verbose",              "Enable verbose output for plugin installation")
                    }.Concat(SharedCommands).ToList(),
                    "<name>"
                ),
                new Command(
                    "license",
                    "install a license for a proprietary Vagrant plugin",
                    new List<Command>().Concat(SharedCommands).ToList(),
                    "<name> <license-file>"
                ),
                new Command(
                    "list",
                    "list all installed plugins and their respective installed versions",
                    new List<Command>(){
                        new Command("--local", "Include local project plugins")
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "repair",
                    "attempt to automatically repair problems with plugins",
                    new List<Command>(){
                        new Command("--local", "Repair plugins in local project")
                    }.Concat(SharedCommands).ToList()
                ),
                new Command(
                    "uninstall",
                    "uninstall the plugin with the given name",
                    new List<Command>(){
                        new Command("--local", "Remove plugin from local project")
                    }.Concat(SharedCommands).ToList(),
                    "<name> [<name2> <name3> ...]"
                ),
                new Command(
                    "update",
                    "update the plugins that are installed within Vagrant",
                    new List<Command>(){
                        new Command("--local", "Update plugin in local project")
                    }.Concat(SharedCommands).ToList(),
                    "[<name> <name2> ...]"
                ),
            }.Concat(SharedCommands).ToList());

        #endregion

        #region PortCommand

        private static readonly Command PortCommand = new(
            "port",
            "displays information about guest port mappings",
            new List<Command>() {
                new Command("--guest", "Output the host port that maps to the given guest port", null, "<port>")
            }.Concat(SharedCommands).ToList(),
            "<name|id>"    
        );

        #endregion

        #region PowershellCommand

        private static readonly Command PowershellCommand = new(
            "powershell",
            "connects to machine via powershell remoting",
            new List<Command>() {
                new Command("--command",  "Execute a powershell command directly",                  null, "<command>",               new[] {"-c"}),
                new Command("--elevated", "Execute a powershell command with elevated permissions", null, "",                        new[] {"-e"}),
                new Command("--",         "Send extra powershell arguments",                        null, "<extra_powershell_args>", null,          true)
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region ProviderCommand

        private static readonly Command ProviderCommand = new(
            "provider",
            "show provider for this environment",
            new List<Command>() {
                new Command("--install", "Install the provider if possible"),
                new Command("--usable",  "Checks if the named provider is usable")
            }.Concat(SharedCommands).ToList(),
            "<machine_name>"
        );

        #endregion

        #region ProvisionCommand

        private static readonly Command ProvisionCommand = new(
            "provision",
            "provisions the vagrant machine",
            new List<Command>() {
                new Command("--provision-with", "Enable only certain provisioners, by type or by name", null, "<x,y,z>")
            }.Concat(SharedCommands).ToList(),
            "<vm_name>"
        );

        #endregion

        #region PushCommand

        private static readonly Command PushCommand = new(
            "push",
            "deploys code in this environment to a configured destination",
            new List<Command>().Concat(SharedCommands).ToList(),
            "[provider]"
        );

        #endregion

        #region RdpCommand

        private static readonly Command RdpCommand = new(
            "rdp",
            "connects to machine via RDP",
            new List<Command>() {
                new Command("--", "send extra args for RDP", null, "<extra_args>", null, true)
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region ReloadCommand

        private static readonly Command ReloadCommand = new(
            "reload",
            "restarts vagrant machine, loads new Vagrantfile configuration",
            new List<Command>() {
                new Command("--provision",      "Enable provisioning"),
                new Command("--no-provision",   "Disable provisioning"),
                new Command("--provision-with", "Enable only certain provisioners, by type or by name", null, "<x,y,z>"),
                new Command("--force",          "Force shut down (equivalent of pulling power)",        null, "", new[] {"-f"})
            }.Concat(SharedCommands).ToList(),
            "[vm_name]"
        );

        #endregion

        #region ResumeCommand

        private static readonly Command ResumeCommand = new(
            "resume",
            "resume a suspended vagrant machine",
            new List<Command>() {
                new Command("--provision",      "Enable provisioning"),
                new Command("--no-provision",   "Disable provisioning"),
                new Command("--provision-with", "Enable only certain provisioners, by type or by name", null, "<x,y,z>")
            }.Concat(SharedCommands).ToList(),
            "[vm_name]"
        );

        #endregion

        #region RsyncCommand

        private static readonly Command RsyncCommand = new(
            "rsync",
            "syncs rsync synced folders to remote machine",
            new List<Command>() {
                new Command("--rsync-chown", "Use rsync to modify ownership"),
                new Command("--no-rsync-chown", "Do not use rsync to modify ownership")
            }.Concat(SharedCommands).ToList(),
            "[vm_name]"
        );

        #endregion

        #region RsyncAutoCommand

        private static readonly Command RsyncAutoCommand = new(
            "rsync-auto",
            "syncs rsync synced folders automatically when files change",
            new List<Command>() {
                new Command("--poll", "Force polling filesystem (slow)"),
                new Command("--no-poll", "Do not poll filesystem"),
                new Command("--rsync-chown", "Use rsync to modify ownership"),
                new Command("--no-rsync-chown", "Do not use rsync to modify ownership")
            }.Concat(SharedCommands).ToList(),
            "[vm_name]");

        #endregion

        #region ShareCommand

        private static readonly Command ShareCommand = new(
            "share",
            "Allows anyone in the world with an internet connection to access your Vagrant environment by giving you a globally accessible URL.",
            new List<Command>() {
                new Command("--disable-http",    "Disable publicly visible HTTP(S) endpoint"),
                new Command("--disable-https",   "Disable publicly visible HTTPS endpoint only"),
                new Command("--domain",          "Domain the share name will be a subdomain of", null, "<value>"),
                new Command("--http",            "Local HTTP port to forward to",                null, "<port>"),
                new Command("--https",           "Local HTTPS port to forward to",               null, "<port>"),
                new Command("--name",            "Specific name for the share",                  null, "<name>"),
                new Command("--ssh",             "Allow 'vagrant connect --ssh' access"),
                new Command("--ssh-no-password", "Key won't be encrypted with --ssh"),
                new Command("--ssh-port",        "Specific port for SSH when using --ssh",       null, "<port>"),
                new Command("--ssh-once",        "Allow 'vagrant connect --ssh' only one time"),
                new Command("--driver",          "Deprecated option for compatibility",          null, "<driver>"),
                new Command("--full",            "Share entire machine"),
                new Command("--share-password",  "Custom share password"),
            }.Concat(SharedCommands).ToList()
            
        );

        #endregion

        #region SnapshotCommand

        private static readonly Command SnapshotCommand = new(
            "snapshot",
            "manages snapshots: saving, restoring, etc.",
            new List<Command>() {
                new Command(
                    "delete",
                    "Delete a snapshot taken previously with snapshot save",
                    new List<Command>().Concat(SharedCommands).ToList(),
                    "[vm_name] <snapshot_name>"
                ),
                new Command(
                    "list",
                    "List all snapshots taken for a machine",
                    new List<Command>().Concat(SharedCommands).ToList(),
                    "[vm_name]"
                ),
                new Command(
                    "pop",
                    "Restore state that was pushed onto the snapshot stack with `vagrant snapshot push`.",
                    new List<Command>() {
                        new Command("--provision",      "Enable provisioning"),
                        new Command("--no-provision",   "Disable provisioning"),
                        new Command("--provision-with", "Enable only certain provisioners, by type or by name", null, "<x,y,z>"),
                        new Command("--no-delete",      "Don't delete the snapshot after the restore"),
                        new Command("--no-start",       "Don't start the snapshot after the restore")
                    }.Concat(SharedCommands).ToList(),
                    "[vm_name]"
                ),
                new Command(
                    "push",
                    "Take a snapshot of the current state of the machine and 'push' it onto the stack of states.",
                    new List<Command>().Concat(SharedCommands).ToList(),
                    "[vm-name]"
                ),
                new Command(
                    "restore",
                    "Restore a snapshot taken previously with snapshot save.",
                    new List<Command>() {
                        new Command("--provision",      "Enable provisioning"),
                        new Command("--no-provision",   "Disable provisioning"),
                        new Command("--provision-with", "Enable only certain provisioners, by type or by name", null, "<x,y,z>"),
                        new Command("--no-start",       "Don't start the snapshot after the restore")
                    }.Concat(SharedCommands).ToList(),
                    "[vm_name] <snapshot_name>"
                ),
                new Command(
                    "save",
                    "Take a snapshot of the current state of the machine",
                    new List<Command>() {
                        new Command("--force", "Replace snapshot without confirmation", null, "", new[] {"-f"})
                    }.Concat(SharedCommands).ToList(),
                    "[vm_name] <snapshot_name>"
                ),
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region SshCommand

        private static readonly Command SshCommand = new(
            "ssh",
            "connects to machine via SSH",
            new List<Command>() {
                new Command("--",        "Add extra SSH arguments",                                      null, "<extra_args>", null, true),
                new Command("--command", "Execute an SSH command directly",                              null, "<command>",    new[] {"-c"}),
                new Command("--plain",   "Plain mode, leaves authentication up to user",                 null, "",             new[] {"-p"}),
                new Command("--tty",     "Enables tty when executing an ssh command (defaults to true)", null, "",             new[] {"-t"}),
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region SshConfigCommand

        private static readonly Command SshConfigCommand = new(
            "ssh-config",
            "outputs OpenSSH valid configuration to connect to the machine",
            new List<Command>() {
                new Command("--host", "Name the host for the config", null, "<name>")
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region StatusCommand

        private static readonly Command StatusCommand = new(
            "status",
            "outputs status of the vagrant machine",
            new List<Command>().Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region SuspendCommand

        private static readonly Command SuspendCommand = new(
            "suspend",
            "suspends the machine",
            new List<Command>() {
                new Command("--all-global", "Suspend all running vms globally", null, "", new[] {"-a"})
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region UpCommand

        private static readonly Command UpCommand = new(
            "up",
            "starts and provisions the vagrant environment",
            new List<Command>() {
                new Command("--provision",           "Enable provisioning"),
                new Command("--no-provision",        "Disable provisioning"),
                new Command("--provision-with",      "Enable only certain provisioners, by type or by name", null, "<x,y,z>"),
                new Command("--destroy-on-error",    "Destroy machine if any fatal error happens (default to true)"),
                new Command("--no-destroy-on-error", "Do not destroy machine if any fatal error happens"),
                new Command("--parallel",            "Enable parallelism if provider supports it"),
                new Command("--no-parallel",         "Disable parallelism"),
                new Command("--provider",            "Back the machine with a specific provider", null, "<provider>"),
                new Command("--install-provider",    "If possible, install the provider if it isn't installed"),
                new Command("--no-install-provider", "Do not install the provider if it isn't installed")
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region UploadCommand

        private static readonly Command UploadCommand = new(
            "upload",
            "upload to machine via communicator",
            new List<Command>() {
                new Command("--temporary",        "Upload source to temporary directory",  null, "",          new[] {"-t"}),
                new Command("--compress",         "Use gzip compression for upload",       null, "",          new[] {"-c"}),
                new Command("--compression-type", "Type of compression to use (tgz, zip)", null, "<tgz|zip>", new[] {"-C"})
            }.Concat(SharedCommands).ToList(),
            "<source> [destination] [name|id]"
        );

        #endregion

        #region ValidateCommand

        private static readonly Command ValidateCommand = new(
            "validate",
            "validates the Vagrantfile",
            new List<Command>() {
             new Command("--ignore-provider", "Ignores provider config options", null, "", new[] {"-p"})
            }.Concat(SharedCommands).ToList()
        );

        #endregion

        #region VersionCommand

        private static readonly Command VersionCommand = new(
            "version",
            "prints current and latest Vagrant version",
            new List<Command>().Concat(SharedCommands).ToList()
        );

        #endregion

        #region WinrmCommand

        private static readonly Command WinrmCommand = new(
            "winrm",
            "executes commands on a machine via WinRM",
            new List<Command>() {
                new Command("--command",  "Execute a WinRM command directly",      null, "<command>",        new[] {"-c"}),
                new Command("--elevated", "Run with elevated credentials",         null, "",                 new[] {"-e"}),
                new Command("--shell",    "Use specified shell (powershell, cmd)", null, "<powershell|cmd>", new[] {"-s"})
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        #region WinrmConfigCommand

        private static readonly Command WinrmConfigCommand = new(
            "winrm-config",
            "outputs WinRM configuration to connect to the machine",
            new List<Command>() {
                new Command("--host", "Name the host for the config", null, "<name>")
            }.Concat(SharedCommands).ToList(),
            "[name|id]"
        );

        #endregion

        public static readonly Command VagrantCommand = new(
            "vagrant",
            "A tool for building and managing virtual machine environments in a single workflow.",
            new List<Command>() {
                AutocompleteCommand,
                BoxCommand,
                CapCommand,
                CloudCommand,
                ConnectCommand,
                DestroyCommand,
                DockerExecCommand,
                DockerLogsCommand,
                DockerRunCommand,
                GlobalStatusCommand,
                HaltCommand,
                HelpCommand,
                InitCommand,
                ListCommandsCommand,
                LoginCommand,
                PackageCommand,
                PluginCommand,
                PortCommand,
                PowershellCommand,
                ProviderCommand,
                ProvisionCommand,
                PushCommand,
                RdpCommand,
                ReloadCommand,
                ResumeCommand,
                RsyncCommand,
                RsyncAutoCommand,
                ShareCommand,
                SnapshotCommand,
                SshCommand,
                SshConfigCommand,
                StatusCommand,
                SuspendCommand,
                UpCommand,
                UploadCommand,
                ValidateCommand,
                VersionCommand,
                WinrmCommand,
                WinrmConfigCommand,
            }.Concat(SharedCommands).ToList(),
            altCommandStrings: new[] {"vagrant.exe"}
        );
    }
}
