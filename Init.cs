﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Subsystem;
using System.Text;
using System.Threading.Tasks;

namespace VagrantPredictor {
    public class Init : IModuleAssemblyInitializer, IModuleAssemblyCleanup {
        private const string Identifier = "e48e1fd9-e5f5-4e10-b83f-4d6d56dd13fa";

        /// <summary>
        /// Gets called when assembly is loaded.
        /// </summary>
        public void OnImport() {
            var predictor = new VagrantPredictor(Identifier);
            SubsystemManager.RegisterSubsystem(SubsystemKind.CommandPredictor, predictor);
        }

        /// <summary>
        /// Gets called when the binary module is unloaded.
        /// </summary>
        public void OnRemove(PSModuleInfo psModuleInfo) {
            SubsystemManager.UnregisterSubsystem(SubsystemKind.CommandPredictor, new Guid(Identifier));
        }
    }
}
