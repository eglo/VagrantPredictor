# VagrantPredictor

The `VagrantPredictor` is a [PSReadLine](https://github.com/PowerShell/PSReadLine) [Predictive Intellisense](https://devblogs.microsoft.com/powershell/announcing-psreadline-2-1-with-predictive-intellisense/) plugin for the [Vagrant](https://vagrantup.com) application.

![Example](/res/img/VagrantPredictorExample1.gif)

Please use this as an example to build a predictor yourself! The [Command](/Command.cs) class was built to be fairly extensible to other predictors.

# Requirements

- [PowerShell 7.2](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell?view=powershell-7.2)+
- [PSReadLine 2.2.2](https://www.powershellgallery.com/packages/PSReadLine/2.2.2)+

# Installation and Usage
1. Install the module from the PSGallery

```powershell
Install-Module -Name VagrantPredictor -Repository PSGallery
```

2. Import the module and ensure that `PSReadLine` is using plugins for predictions.

```powershell
Import-Module -Name VagrantPredictor
Set-PSReadLineOption -PredictionSource HistoryAndPlugin
```

3. Switch between the `ListView` and `InlineView` by setting the `PredictionViewStyle` or by pressing `F2`

```powershell
Set-PSReadLineOption -PredictionViewStyle ListView
```

# Build
## Build Requirements
- [.NET 6.0 SDK](https://dotnet.microsoft.com/en-us/download)

## Process

1. Clone this repo
2. `cd` to the repo folder
3. Run `dotnet build` in the project directory
4. Import the `.dll`

Optional:
- This predictor works best in `ListView`, which you can swap between with `F2` or by setting the `PSReadLine` option.

```powershell
git clone https://github.com/nixuno/VagrantPredictor.git
cd VagrantPredictor
dotnet build
Import-Module .\bin\Debug\net6.0\VagrantPredictor.dll
Set-PSReadLineOption -PredictionSource HistoryAndPlugin
Set-PSReadLineOption -PredictionViewStyle ListView # Optional
Set-PSReadLineOption -PredictionSource Plugin # Best for testing
```
