﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VagrantPredictor {
    public class Command {
        public Command? ParentCommand { get; set; }
        public string CommandString { get; set; }
        public string Description { get; set; }
        public List<Command> SubCommands { get; set; }
        public string InputHint { get; set; }
        public string[] AltCommandStrings { get; set; }
        public bool IsTerminating { get; set; }

        public Command(string commandString, string description, List<Command>? subCommands = null, string inputHint = "", string[]? altCommandStrings = null, bool isTerminating = false) {
            CommandString = commandString;
            Description = description;
            SubCommands = subCommands ?? new List<Command>();
            if (SubCommands.Count > 0) {
                foreach (Command subCommand in SubCommands) {
                    subCommand.ParentCommand = this;
                }
            }
            AltCommandStrings = altCommandStrings ?? Array.Empty<string>();
            InputHint = inputHint;
            IsTerminating = isTerminating;
        }
    }
}
